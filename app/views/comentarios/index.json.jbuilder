json.array!(@comentarios) do |comentario|
  json.extract! comentario, :id, :autor, :titulo, :fecha
  json.url comentario_url(comentario, format: :json)
end
